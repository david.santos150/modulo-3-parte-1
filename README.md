Marcação básica
M1. Cada elemento de cada página á marcado. 

M2. O repositório tem um arquivo de estilos (ou dois, se usa o normalize.css).

M3. O arquivo de estilos está habilitado na tag <head>.

M4. Não há erros graves na marcação.

Estilização
E1. Toda estilização (exceto normalise.css)  é feita num arquivo único.

E2. A localização dos blocos é descrita com os flexboxes.

E3. No CSS não usa-se !important.

E4. As fontes habilitadas correspondem ao desenho, têm os mesmos tamanhos, cores, estilos; tamanhos das alturas de linhas são corretas. 

E5. Há uma fonte alternativa e sua font-family.

Outros
O1. No repositório há pastas css, img, js ou análogas. A página principal tem o nome index.html. Em nomes não há espaços vazios, letras maiúsculas, só caracteres latin.

O2. A formatação/estilização/indexação de código é consistente entre os arquivos HTML, CSS, JS.

O3. As imagens têm formatos corretos.
